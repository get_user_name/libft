/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 04:16:35 by gkoch             #+#    #+#             */
/*   Updated: 2018/11/20 04:29:14 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

static int	ft_count_s(char const *s, char c)
{
	int		item;
	int		n;

	item = 0;
	n = 0;
	while (s[n])
	{
		while (s[n] == c && s[n])
			n++;
		while (s[n] != c && s[n])
			n++;
		item++;
	}
	return (item);
}

static void	ft_adds(char *answer, char const *str, int end)
{
	int		n;

	n = -1;
	while (++n < end)
		answer[n] = str[n];
	answer[n] = '\0';
}

char		**ft_strsplit(char const *s, char c)
{
	char	**answer;
	int		item;
	int		n;
	int		start;

	if (!s || !c)
		return (NULL);
	answer = (char**)malloc(sizeof(char*) * (ft_count_s(s, c) + 1));
	item = 0;
	n = 0;
	while (s[n])
	{
		while (s[n] == c && s[n])
			n++;
		start = n;
		while (s[n] != c && s[n])
			n++;
		answer[item] = ft_strnew((size_t)(n - start + 1));
		ft_adds(answer[item], s + start, n - start);
		item++;
	}
	answer[item] = NULL;
	return (answer);
}
