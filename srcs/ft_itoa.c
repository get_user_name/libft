/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 03:02:38 by gkoch             #+#    #+#             */
/*   Updated: 2018/11/20 03:09:21 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

static int	ft_lengthint(int n)
{
	int		i;

	i = 0;
	if (n < 0)
	{
		i++;
		n *= -1;
	}
	while (n)
	{
		n /= 10;
		i++;
	}
	return (i);
}

static char	*ft_getitem(int i)
{
	char	*answer;
	char	*minint;
	int		n;

	answer = NULL;
	minint = "-2147483648";
	n = -1;
	if (i)
	{
		answer = ft_strnew(11);
		if (!answer)
			return (NULL);
		while (++n < 12)
			answer[n] = minint[n];
		return (answer);
	}
	else
	{
		answer = ft_strnew(2);
		if (!answer)
			return (NULL);
		answer[0] = '0';
		answer[1] = '\0';
		return (answer);
	}
}

char		*ft_itoa(int n)
{
	char	*answer;
	int		flag;
	int		i;

	if (n == 0)
		return (ft_getitem(0));
	if (n == -2147483648)
		return (ft_getitem(1));
	if (!(answer = ft_strnew((size_t)(ft_lengthint(n) + 1))))
		return (NULL);
	flag = 0;
	i = 0;
	n < 0 ? (flag = 1) : n;
	n < 0 ? (n *= -1) : n;
	while (n)
	{
		answer[i++] = (char)((n % 10) + '0');
		n /= 10;
	}
	flag ? (answer[i++] = '-') : flag;
	answer[i] = '\0';
	answer = ft_strrev(answer);
	return (answer);
}
