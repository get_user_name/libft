/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 03:16:39 by gkoch             #+#    #+#             */
/*   Updated: 2018/11/20 03:17:12 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	*ft_memccpy(void *dest, const void *src, int c, size_t num)
{
	size_t		n;
	char		*str1;
	const char	*str2;

	str1 = (char*)dest;
	str2 = (char*)src;
	n = 0;
	if (dest == NULL || src == NULL)
		return (NULL);
	while (n < num)
	{
		if ((int)str2[n] != c)
			str1[n] = str2[n];
		else
			return ((char*)dest + n);
		n++;
	}
	return (NULL);
}
