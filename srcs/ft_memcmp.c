/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 03:18:17 by gkoch             #+#    #+#             */
/*   Updated: 2018/11/20 03:18:57 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

int		ft_memcmp(const void *str1, const void *str2, size_t count)
{
	const	char	*s1;
	const	char	*s2;
	size_t			n;

	s1 = (const char*)str1;
	s2 = (const char*)str2;
	n = 0;
	while (n < count)
	{
		if (*s1 != *s2)
			return (*s1 - *s2);
		s1++;
		s2++;
		n++;
	}
	return ((unsigned char)*s1 - (unsigned char)*s2);
}
