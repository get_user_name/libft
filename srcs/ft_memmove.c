/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 03:27:42 by gkoch             #+#    #+#             */
/*   Updated: 2018/11/20 03:27:43 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	*ft_memmove(void *dest, const void *src, size_t count)
{
	char	*str1;
	char	*str2;
	char	*strmod;
	size_t	n;

	str1 = (char*)src;
	str2 = (char*)dest;
	strmod = (char*)malloc(sizeof(char) * ft_strlen((char*)dest));
	n = 0;
	if (str1 == NULL || str2 == NULL)
		return (NULL);
	while (n < count)
	{
		strmod[n] = str1[n];
		n++;
	}
	n = 0;
	while (n < count)
	{
		str2[n] = strmod[n];
		n++;
	}
	return (dest);
}
