/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 03:28:57 by gkoch             #+#    #+#             */
/*   Updated: 2018/11/20 03:29:07 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	ft_memset(void *s, int c, size_t count)
{
	size_t	n;
	char	*str;

	str = (char *)s;
	n = 0;
	if (s == NULL)
		return ;
	while (n < count)
	{
		str[n] = c;
		n++;
	}
}
