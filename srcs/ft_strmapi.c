/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 04:11:06 by gkoch             #+#    #+#             */
/*   Updated: 2018/11/20 04:11:19 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char	*answer;
	int		n;

	answer = ft_strnew((size_t)(ft_strlen(s) + 1));
	if ((s == NULL) || (f == NULL) || !answer)
		return (NULL);
	n = 0;
	while (s[n])
	{
		answer[n] = f(n, s[n]);
		n++;
	}
	return (answer);
}
