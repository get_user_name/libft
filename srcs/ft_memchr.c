/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 03:17:23 by gkoch             #+#    #+#             */
/*   Updated: 2018/11/20 03:17:52 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	*ft_memchr(const void *s, int c, size_t num)
{
	size_t		n;
	const char	*str;

	str = (const char*)s;
	n = 0;
	if (s == NULL)
		return (NULL);
	while (n < num)
	{
		if ((int)str[n] == c)
			return ((char*)s + n);
		n++;
	}
	return (NULL);
}
