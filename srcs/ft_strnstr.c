/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 04:14:22 by gkoch             #+#    #+#             */
/*   Updated: 2018/11/20 04:14:59 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strnstr(const char *str, const char *to_find, size_t n)
{
	int		i;
	int		j;
	char	*str1;
	char	*str2;

	str1 = (char*)str;
	str2 = (char*)to_find;
	i = 0;
	if (str2[0] == '\0')
		return (str1);
	while (str1[i] && (n > 0))
	{
		j = 0;
		if (str1[i] == str2[j])
		{
			while (str2[j] && (str2[j] == str1[i + j]))
				j++;
			if (str2[j] == '\0')
				return (str1 + i);
		}
		if (!--n)
			break ;
		i++;
	}
	return (NULL);
}
