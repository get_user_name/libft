/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 04:19:18 by gkoch             #+#    #+#             */
/*   Updated: 2018/11/20 04:19:50 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

static	int	ft_getitem(const char *s)
{
	int		item;

	item = 0;
	while ((s[item] == 32) ||
		(s[item] == 9) ||
		(s[item] == 10))
		item++;
	return (item);
}

char		*ft_strtrim(char const *s)
{
	char	*answer;
	char	*revears;
	int		start;
	int		end;
	int		n;

	if (s == NULL)
		return (NULL);
	revears = ft_strrev(s);
	start = ft_getitem(s);
	end = ft_strlen(s) - ft_getitem(revears);
	ft_strdel(&revears);
	answer = ft_strnew(end - start + 1);
	if (!answer)
		return (NULL);
	n = 0;
	while (start < end)
		answer[n++] = s[start++];
	answer[n] = '\0';
	return (answer);
}
