/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 04:15:48 by gkoch             #+#    #+#             */
/*   Updated: 2018/11/20 04:16:15 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*ft_strrev(const char *s)
{
	char	*strrev;
	int		start;
	int		end;

	start = 0;
	end = ft_strlen(s);
	strrev = ft_strnew(end + 1);
	while (--end >= 0)
		strrev[start++] = s[end];
	strrev[start] = '\0';
	return (strrev);
}
