/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 04:08:38 by gkoch             #+#    #+#             */
/*   Updated: 2018/11/20 04:11:00 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

char	*t_strmap(char const *s, char (*f)(char))
{
	char	*answer;
	int		n;

	answer = ft_strnew((size_t)(ft_strlen(s) + 1));
	if ((s == NULL) || (f == NULL) || !answer)
		return (NULL);
	n = 0;
	while (s[n])
	{
		answer[n] = f(s[n]);
		n++;
	}
	return (answer);
}
