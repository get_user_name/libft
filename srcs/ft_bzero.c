/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 03:00:42 by gkoch             #+#    #+#             */
/*   Updated: 2018/11/20 03:00:57 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	ft_bzero(void *s, size_t count)
{
	size_t	n;
	char	*str;

	str = (char *)s;
	n = 0;
	if (s == NULL)
		return ;
	while (n < count)
	{
		str[n] = '\0';
		n++;
	}
}
