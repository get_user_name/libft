/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 03:19:06 by gkoch             #+#    #+#             */
/*   Updated: 2018/11/20 03:19:48 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

void	*ft_memcpy(void *destptr, const void *srcptr, size_t num)
{
	size_t		n;
	char		*str1;
	const char	*str2;

	str1 = (char*)destptr;
	str2 = (char*)srcptr;
	n = 0;
	if (destptr == NULL || srcptr == NULL)
		return (NULL);
	while (n < num)
	{
		str1[n] = str2[n];
		n++;
	}
	return (destptr);
}
